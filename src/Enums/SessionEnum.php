<?php
/**
 * Created by PhpStorm.
 * User: Oleg G.
 * Date: 27.05.2018
 * Time: 10:40
 */

namespace PhpExt\Session\Enums;

use MyCLabs\Enum\Enum;

class SessionEnum extends Enum
{
    const MESSAGE = 'message';
    const ERROR = 'error';
}